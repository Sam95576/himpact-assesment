import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getUserList() {
    return this.http.get(`https://reqres.in/api/users?page=1`)
  }

  getUserDetail(_id) {
    return this.http.get(`https://reqres.in/api/users/${_id}`)
  }
}
