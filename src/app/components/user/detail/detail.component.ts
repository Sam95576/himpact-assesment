import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  public dId: any
  public UserDetaiData: any;

  constructor(private route: ActivatedRoute,
    private _userService: UserService
  ) { }

  ngOnInit(): void {
    this.dId = this.route.snapshot.paramMap.get('id');
    this.getUserDetail()
  }


  getUserDetail() {
    this._userService.getUserDetail(this.dId).subscribe(e => {
      if (e['data']) {
        this.UserDetaiData = e['data']
      }
    })
  }
}
