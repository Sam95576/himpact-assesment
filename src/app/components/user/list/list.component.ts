import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  public userData: any = []


  constructor(
    private _userService: UserService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getUserList()
  }

  getUserList() {
    this._userService.getUserList().subscribe(e => {
      if (e['data'].length > 0) {
        this.userData = e['data']
        this.userData.sort((a, b) => a.first_name.localeCompare(b.first_name))
      }
    })
  }

  goToUserDetail(value) {
    this.router.navigate(['/user/detail', value])

  }
}
